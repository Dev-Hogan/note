import addTag from '$/add-tag.svg'
import addQuestion from '$/add-question.svg'
import allQuestion from '$/all-question.svg'
import all from '$/all.svg'
import answer from '$/answer.svg'
import arrowBottom from '$/arrow-bottom.svg'
import arrowLeft from '$/arrow-left.svg'
import arrowLeft2x from '$/arrow-left@2x.svg'
import arrowRight from '$/arrow-right.svg'
import arrowTop from '$/arrow-top.svg'
import ask1 from '$/ask1.svg'
import ask2 from '$/ask2.svg'
import ask3 from '$/ask3.svg'
import ask4 from '$/ask4.svg'
import checked from '$/checked.svg'
import close from '$/close.svg'
import collapse from '$/collapse.svg'
import copy from '$/copy.svg'
import edit from '$/edit.svg'
import focus from '$/focus.svg'
import focus2 from '$/focus2.svg'
import focus3 from '$/focus3.svg'
import focus4 from '$/focus4.svg'
import fullscreen1 from '$/fullscreen1.svg'
import quotation from '$/quotation.svg'
import info from '$/info.svg'
import link from '$/link.svg'
import link2 from '$/link2.svg'
import more from '$/more.svg'
import pdf2 from '$/pdf2.svg'
import random from '$/random.svg'
import search from '$/search.svg'
import setting2 from '$/setting2.svg'
import share from '$/share.svg'
import sort from '$/sort.svg'
import sort2 from '$/sort2.svg'
import tag from '$/tag.svg'
import trash from '$/trash.svg'
import unfocus from '$/unfocus.svg'
import { IconMap } from '@/models'
import bold from '$/bold.svg'

export const iconMap: IconMap = {
  addTag: addTag,
  addQuestion: addQuestion,
  all: all,
  answer: answer,
  arrowBottom: arrowBottom,
  arrowLeft: arrowLeft,
  arrowLeft2x: arrowLeft2x,
  arrowRight: arrowRight,
  arrowTop: arrowTop,
  ask1: ask1,
  ask2: ask2,
  ask3: ask3,
  ask4: ask4,
  checked: checked,
  close: close,
  collapse: collapse,
  copy: copy,
  edit: edit,
  focus: focus,
  focus2: focus2,
  focus3: focus3,
  focus4: focus4,
  fullscreen1: fullscreen1,
  quotation: quotation,
  info: info,
  link: link,
  link2: link2,
  more: more,
  pdf2: pdf2,
  search: search,
  setting2: setting2,
  share: share,
  sort: sort,
  tag: tag,
  trash: trash,
  unfocus: unfocus,
  random: random,
  sort2: sort2,
  allQuestion: allQuestion,
  bold: bold
}
