import { category } from "@/mock";

export enum Routes {
  dev = 'dev',
  pageNotFound = 'pageNotFound',
  home = 'home',

  all = 'all',
  focus = 'focus',
  waitingAnswer = 'waitingAnswer',
  randomLook = 'randomLook',
  tags = 'tags',
  wastebasket = 'wastebasket',

  category = 'category',
}

export enum ThemeMode {
  light = 'light',
  dark = 'dark'
}
